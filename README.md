# Atomic Sample Creation
This is a container that can run atomsk and supercell commands. The pipeline runs automatically when changing the file "input.sh". This is just a bash script file that will execute when you change the file and commit it. Under the tab "CI/CD" in the sidebar in the "[Jobs](https://gitlab.com/tfriedrich/atomic_sample_creation/-/jobs)" Menu the log of this pipeline and the output files can be found. The script can take most common bash script commands, as well as [atomsk](https://atomsk.univ-lille.fr/index.php) and [supercell](https://orex.github.io/supercell/) commands. These two CLI-programs very useful for atomic model creation. Atomsk allows the creation and modification of atomic models and supports a large number of file formats, including cif. Supercell allows to create structure configurations for disordered crystals (Occupation probabilities result in a number of possible configurations for finite crystals). Refer to the corresponding documentations to learn about the commands. Here are some example codes that illustrate common usages. These commands can just be copy/pasted into the input.sh script.

## Create atomic structure from scratch
```
atomsk --create bcc 2.85 Fe Fe.xsf
```
![Result](Figures/Fe.png)
*Fe unit cell in [001] Zone axis orientation*

## Create atomic structure and transform it into a given orientation
```
atomsk --create bcc 2.85 Fe Fe.xsf                                          # create structure with lattice vectors aligned to cartesian axis
atomsk Fe.xsf -orient [100] [010] [001] [121] [-101] [1-11] Fe_orient.xsf   # change orientation from [100] [010] [001] to [121] [-101] [1-11]
atomsk Fe_orient.xsf -duplicate 2 2 2 xyz                                   # Convert to xyz coordinates
```
![Result](Figures/Fe_orient.png)
*Fe unit cell in [111] Zone axis orientation*

## Create supercell configurations from disordered crystal
```
supercell -i NaYF4_Hexagonal_SpaceGroupNo174.cif -s 1x1x2 -n r5 -v 2 -o NaYF4_174_222sc -m  # Create two possible configurations
atomsk NaYF4_174_222sc_ir0_w2.cif -duplicate 12 12 3 sc1.cif                                # Expand the models
atomsk NaYF4_174_222sc_ir1_w2.cif -duplicate 12 12 3 sc2.cif
atomsk sc1.cif -cut below 10 x -cut above 40 x -cut below 10 y -cut above 40 y final1.xyz   # Crop the model into squares and export as .xyz
atomsk sc2.cif -cut below 10 x -cut above 40 x -cut below 10 y -cut above 40 y final2.xyz   # Crop the model into squares and export as .xyz
```

*Unit cell of disordered structure*

| Structure configuration 1 | Structure configuration 2|
| ------ | ------ |
| ![Disordered structure](Figures/NaYF4_Hexagonal_SpaceGroupNo174.png) *Disordered unit cell* | ![Disordered structure](Figures/NaYF4_Hexagonal_SpaceGroupNo174.png) *Disordered unit cell*|
| ![Supercell 1](Figures/NaYF4_174_222sc_ir0_w2.png) *1x1x2 unit cells of configuration 1*|![Supercell 1](Figures/NaYF4_174_222sc_ir1_w2.png)*1x1x2 unit cells of configuration 2*|
| ![Supercell 1](Figures/final1.png) *Final structure of configuration 1*| ![Supercell 2](Figures/final2.png) *Final structure of configuration 2*| 

## Create a crystal with dislocation

```
atomsk --create fcc 3.9242 Pt orient [110] [-111] [1-12] Pt_uc.xsf
atomsk Pt_uc.xsf -duplicate 60 20 6 Pt_supercell.xsf
atomsk Pt_supercell.xsf -dislocation 0.501*box 0.501*box edge Z Y 2.774828 0.38 Pt_supercell_edge.xyz
atomsk Pt_supercell_edge.xyz -cut below 0.4*box x -cut above 0.6*box x -cut below 0.4*box y -cut above 0.6*box y Pt_edge_cut.xyz
```
![Result](Figures/Pt_edge_cut.png)
*Pt crystal in [1-12] Zone axis orientation with edge dislocation*

## Create a crystal with twin boundary
```
atomsk --create fcc 3.9242 Pt orient [11-2] [111] [-110] -duplicate 20 5 6 Pt_cell.xsf
atomsk Pt_cell.xsf -mirror 0 Y -wrap Pt_mirror.xsf
atomsk --merge Y 2 Pt_cell.xsf Pt_mirror.xsf Pt_twin.xsf
atomsk Pt_twin.xsf -cut below 0.3*box x -cut above 0.7*box x -cut below 0.25*box y -cut above 0.75*box y Pt_twin_cut.xyz
```
![Result](Figures/Pt_twin_cut.png)
*Pt crystal in [-110] Zone axis orientation with twin boundary*
