atomsk --create fcc 3.9242 Pt orient [11-2] [111] [-110] -duplicate 20 5 6 Pt_cell.xsf
atomsk Pt_cell.xsf -mirror 0 Y -wrap Pt_mirror.xsf
atomsk --merge Y 2 Pt_cell.xsf Pt_mirror.xsf Pt_twin.xsf